package webPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.webDriverPage;

public class SignUpPage extends webDriverPage {

    public SignUpPage(WebDriver webDriver) {
        super(webDriver);
    }

    private final By accBtn           =   By.xpath("//a[contains(text(),'Accept')]");
    private final By signUpBtn        =   By.xpath("//a[@class='signup button']");
    private final By createAccountBtn =   By.xpath("//button[@type='submit']");
    private final By mailtxtbox       =   By.id("email");
    private final By passwordField    =   By.id("password");
    private final By homePage         =   By.xpath("//span[@class='PageHeaderBreadcrumbButton-link-30Jgzu']");
    private final By txtError         =   By.xpath("//*[@id=\"plex\"]/div/div/div/div[1]/span[1]");

    public String signUp(String mailadres, String strongPassword) throws InterruptedException {
        String message;
        WebDriverWait wait = new WebDriverWait(webDriver, 10);

        String mailInsert = mailadres;
        //popup Accept

        Thread.sleep(2000);
        WebElement acceptBtn = webDriver.findElement(accBtn);
        if(acceptBtn.isDisplayed()){
        acceptBtn.click();
        }

        // Signup knop
        wait.until(ExpectedConditions.elementToBeClickable(signUpBtn));
        WebElement BtnSignUp = webDriver.findElement(signUpBtn);
        BtnSignUp.click();

        // iFrame
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("fedauth-iFrame"));

        // input email
        wait.until(ExpectedConditions.elementToBeClickable(mailtxtbox));
        WebElement mailBox = webDriver.findElement(mailtxtbox);
        mailBox.sendKeys(mailadres);

        // input password
        WebElement pwField = webDriver.findElement(passwordField);
        pwField.sendKeys(strongPassword);

        // submit
        WebElement BtnCreateAccount = webDriver.findElement(createAccountBtn);
        BtnCreateAccount.click();

        if(mailInsert=="plex@mailinator.com"){
        wait.until(ExpectedConditions.elementToBeClickable(txtError));
        WebElement error = webDriver.findElement(txtError);
        message = error.getText();
        }else{
        // check homePage
        wait.until(ExpectedConditions.elementToBeClickable(homePage));
        WebElement homePg = webDriver.findElement(homePage);
        message = homePg.getText();
        }

        return message;
    }
}
