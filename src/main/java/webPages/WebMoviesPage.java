package webPages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.webDriverPage;

public class WebMoviesPage extends webDriverPage {

    private static final By onDemandTab      = By.xpath("//a[@href='https://www.plex.tv/watch-free/']");
    private static final By moviesShowsTab   = By.xpath("//a[@href='https://mediaverse.plex.tv']");
    private static final By searchBox        = By.xpath("//*[@id='query']");
    private static final By resultItem       = By.xpath("//*[@id='app']/div/div[2]/div[1]/div[2]/div/ul/li/a");

    public WebMoviesPage(WebDriver webDriver) {
        super(webDriver);
    }

    public static void GoToMovies(WebDriver webDriver){
        // add wait
        WebDriverWait wait = new WebDriverWait(webDriver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(onDemandTab));

        //Go to movies via header (on demand - Movies & Shows)
        Actions action = new Actions(webDriver);
        WebElement onDemand = webDriver.findElement(onDemandTab);
        WebElement moviesShows = webDriver.findElement(moviesShowsTab);
        action.moveToElement(onDemand).moveToElement(moviesShows).click().build().perform();
    }

    public static void SearchMovie (String movieName, WebDriver webDriver){
        // Wait
        WebDriverWait wait = new WebDriverWait(webDriver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(searchBox));

        // Geef zoekterm (filmnaam) in
        WebElement searchField = webDriver.findElement(searchBox);
        searchField.sendKeys(movieName);
        searchField.sendKeys(Keys.ENTER);

        // Wait
        wait.until(ExpectedConditions.elementToBeClickable(resultItem));

        // Click op resultaat
        WebElement searchResult = webDriver.findElement(resultItem);
        searchResult.click();
    }

    public static String SaveMovieTitle(WebDriver webDriver){
        WebElement moviePage = webDriver.findElement(By.xpath("//*[@id=\"app\"]/div/div[2]/div[1]/h1"));
        String movieTitleAdd = moviePage.getText();
        return movieTitleAdd;
    }


}
