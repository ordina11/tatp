package webPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.webDriverPage;


public class LoginPage extends webDriverPage {

    private final String mailadres = "plex-user@mailinator.com";
    private final String pw = "password2020";

    private final By btnAccept = By.xpath("//a[contains(text(),'Accept')]");
    private final By btnSignIn = By.xpath("//a[@class='signin']");
    private final By btnSubmit = By.xpath("//button[@type='submit']");
    private final By tbMail = By.id("email");
    private final By tbPassword = By.id("password");

    public LoginPage(WebDriver webDriver) {
        super(webDriver);
    }

    public void login(){

        // Wacht & Click op Accept Btn
        WebDriverWait wait = new WebDriverWait(webDriver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(btnAccept));
        WebElement acceptBtn = webDriver.findElement(btnAccept);
        acceptBtn.click();

        // Wacht
        wait.until(ExpectedConditions.elementToBeClickable(btnSignIn));

        // SignIn knop
        WebElement signInBtn = webDriver.findElement(btnSignIn);
        signInBtn.click();

        // iFrame
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("fedauth-iFrame"));
     //   webDriver.switchTo().frame("fedauth-iFrame");

        // input email
        wait.until(ExpectedConditions.elementToBeClickable(tbMail));
        WebElement mailtxtbox = webDriver.findElement(tbMail);
        mailtxtbox.sendKeys(mailadres);

        // input password
        WebElement passwordField = webDriver.findElement(tbPassword);
        passwordField.sendKeys(pw);

        // submit
        WebElement createAccountBtn = webDriver.findElement(btnSubmit);
        createAccountBtn.click();
    }
}
