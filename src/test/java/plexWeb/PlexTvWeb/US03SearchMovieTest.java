package plexWeb.PlexTvWeb;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import webPages.LoginPage;
import webPages.WebMoviesPage;
import plexWeb.utils.BaseTests;
import static org.junit.Assert.assertEquals;
import static plexWeb.utils.Screenshot.screenshot;

public class US03SearchMovieTest extends BaseTests {
    /* US3. As a user, I want to be able to search for a specific movie.
        o	Context: A user should be able to perform a search using the search field in the header of the web application.
        o	Acceptance criteria:
            	Suggestions will be shown related to the entered search criteria
            	Clicking on a suggestion will open the correct detail page     */

    @Test
    public void SearchMovie() throws InterruptedException {
        WebDriver webDriver = getWebDriver();
        String movieName = "threesome";
        String movieNameFull = "Threesome (2011)";
        String movieAdded;

        LoginPage login = new LoginPage(webDriver);
        login.login();

        WebMoviesPage moviesPage = new WebMoviesPage(webDriver);
        moviesPage.GoToMovies(webDriver);
        moviesPage.SearchMovie(movieName, webDriver);
        movieAdded = WebMoviesPage.SaveMovieTitle(webDriver);

        assertEquals(movieAdded,movieNameFull);
        screenshot(webDriver, "US03-SearchMovie");
      }


}
