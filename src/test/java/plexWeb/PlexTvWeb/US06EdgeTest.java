package plexWeb.PlexTvWeb;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;

import static org.junit.Assert.assertEquals;
import static plexWeb.utils.Screenshot.screenshot;

public class US06EdgeTest {
    // https://www.toolsqa.com/selenium-webdriver/custom-firefox-profile/
    // https://stackoverflow.com/questions/52464598/how-can-i-set-a-default-profile-for-the-firefox-driver-in-selenium-webdriver-3

    private final String mailadres = "plex-user@mailinator.com";
    private final String pw = "password2020";

    private final By btnAccept = By.xpath("//a[contains(text(),'Accept')]"); // chrome
    //private final By btnAccept = By.xpath("/html/body/div[9]/div/div/div[3]/a[2]"); // <a class="button button-small cookie-consent-accept">I Accept</a>
    private final By btnSignIn = By.xpath("//a[@class='signin']"); // chrome
    //private final By btnSignIn = By.xpath("//*[@id='plex-site-container']/div[3]/div[2]/div[2]/div/div[1]/ul/li[2]/a"); //edge
    private final By btnSubmit = By.xpath("//button[@type='submit']");
    private final By tbMail = By.id("email");
    private final By tbPassword = By.id("password");
    private final By btnOnDemand = By.xpath("//*[@id=\"menu-item-16883\"]/a");

    //*[@id="menu-item-16883"]/a
    private final By menuMoviesShows = By.xpath("//a[@href=\"https://mediaverse.plex.tv\"]");
    private final By liFirstMovie = By.xpath("//*[@id='app']/div/div[2]/div[3]/div[2]/div/ul/li[1]");
    private final By btnWatchlist = By.xpath("//*[@id='app']/div/div[2]/div/div[2]/button[1]");

    WebDriver driver;
    WebDriverWait wait;

    @Before
    public void setup() throws MalformedURLException {
        System.setProperty("webdriver.edge.driver", "C:\\Users\\MeSn\\Documents\\drivers\\msedgedriver86.exe");

        //Creating an object of EdgeDriver
        driver = new EdgeDriver();
        driver.manage().window().maximize();

        //Deleting all the cookies
      //  driver.manage().deleteAllCookies();

        //Specifiying pageLoadTimeout and Implicit wait
        //driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
      //  driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        wait = new WebDriverWait(driver, 10);
        driver.get("https://www.plex.tv/");

    }

    @Test
    public void AddMovie() throws InterruptedException {
    String movieTitleAdd;

    String mailadres = "plex-user@mailinator.com";
    String password = "password2020";

    login();

    // Add movie to WatchList
    movieTitleAdd = addMovieToWatchlist();

    // Open and Check WatchList
    checkWatchList(movieTitleAdd);

    // Click Button: remove from WatchList
    btnRemoveFromWatchList(movieTitleAdd);
    }

    @After
    public void tearDown (){
        driver.close();
    }

    public void login(){
    // Wacht & Click op Accept Btn
   // wait.until(ExpectedConditions.elementToBeClickable(btnAccept));
    WebElement acceptBtn = driver.findElement(btnAccept);
    acceptBtn.click();

    // Wacht
    wait.until(ExpectedConditions.elementToBeClickable(btnSignIn));

    // SignIn knop
    WebElement signInBtn = driver.findElement(btnSignIn);
    signInBtn.click();

    // iFrame
    wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("fedauth-iFrame"));

    // input email
    wait.until(ExpectedConditions.elementToBeClickable(tbMail));
    WebElement mailtxtbox = driver.findElement(tbMail);
    mailtxtbox.sendKeys(mailadres);

    // input password
    WebElement passwordField = driver.findElement(tbPassword);
    passwordField.sendKeys(pw);

    // submit
    WebElement createAccountBtn = driver.findElement(btnSubmit);
    createAccountBtn.click();
    }

    public String addMovieToWatchlist() {

        wait.until(ExpectedConditions.elementToBeClickable(btnOnDemand));
        // Goto movies
        Actions action = new Actions(driver);
        WebElement onDemand = driver.findElement(btnOnDemand);
        WebElement moviesShows = driver.findElement(menuMoviesShows);
        action.moveToElement(onDemand).moveToElement(moviesShows).click().build().perform();

        // Click op eerste film
        wait.until(ExpectedConditions.elementToBeClickable(liFirstMovie));
        WebElement film = driver.findElement(liFirstMovie);
        film.click();

        // Add to WatchList
        WebElement addWatchlist = driver.findElement(btnWatchlist);
        addWatchlist.click();

        // Save movie title added
        // todo (staat ook in MoviesPage)
        WebElement moviePage = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[2]/div[1]/h1"));
        String movieTitleAdd = moviePage.getText();
        return movieTitleAdd;
    }

    public void openMyWatchList(){
        WebElement btnMyWatchlist = driver.findElement(By.xpath("//a[@href=\"/watchlist\"]"));
        btnMyWatchlist.click();

        //Wacht op eerste film
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"app\"]/div/div[2]/ul/li[1]/a")));
    }

    public void checkWatchList (String movieTitleAdd){
       openMyWatchList();

       // open detail pagina van de eerste film in Watchlist
       WebElement myWatchListElement = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[2]/ul/li[1]/a"));
       myWatchListElement.click();

       // wacht op de film title
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"app\"]/div/div[2]/div[1]/h1")));

        // sla de film title op
       WebElement moviePage = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[2]/div[1]/h1"));
       String movieTitleAdded = moviePage.getText();

       assertEquals(movieTitleAdd, movieTitleAdded);
        screenshot(driver, "US06-Edge-checkWatchList");
    }

    public void btnRemoveFromWatchList(String movieTitleAdd){
        // vind remove knop
        WebElement removeBtn = driver.findElement(By.xpath("*//button[@data-testid='Watchlist-button']"));

        //Create object 'action' of an Actions class
        Actions action = new Actions(driver);
        //Mouseover on an element
        action.moveToElement(removeBtn).perform();

        // wacht op tooltip
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"app\"]/div/div[2]/div[1]/div[2]/button[1]/span")));

        WebElement toolTipRemove = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[2]/div[1]/div[2]/button[1]/span"));
        String spanRemove = toolTipRemove.getText();

        assertEquals("Remove From Watchlist", spanRemove);
        screenshot(driver, "US06-Edge-RemoveFromWatchlist");

        // verwijder film van Watchlist
        removeBtn.click();
    }

}
