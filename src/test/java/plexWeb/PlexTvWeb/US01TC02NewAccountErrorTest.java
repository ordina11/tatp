package plexWeb.PlexTvWeb;

import org.junit.Test;
import plexWeb.utils.BaseTests;
import webPages.SignUpPage;

import static org.junit.Assert.assertEquals;

public class US01TC02NewAccountErrorTest extends BaseTests {

    /* US1. As a user, I want to be able to create a new account.
    Context: A new user can create an account by providing his email address and choosing a password.
    Acceptance criteria:
        - After registration, the user is automatically logged in
        - The user is brought to the home page      */

        @Test
        public void NewAccountError(){
            SignUpPage signUpPage = new SignUpPage(getWebDriver());
            String message = null;
            String mailadres = "plex@mailinator.com";
            String strongPassword = "pwMailInator%";

            try {
                message = signUpPage.signUp(mailadres, strongPassword);
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
            assertEquals("Username has already been taken", message);
        }

    }
