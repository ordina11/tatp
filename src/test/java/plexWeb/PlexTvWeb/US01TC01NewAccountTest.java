package plexWeb.PlexTvWeb;

import org.junit.Test;
import plexWeb.utils.BaseTests;
import webPages.SignUpPage;

import static org.junit.Assert.*;

public class US01TC01NewAccountTest extends BaseTests {
    /* US1. As a user, I want to be able to create a new account.
    Context: A new user can create an account by providing his email address and choosing a password.
    Acceptance criteria:
        - After registration, the user is automatically logged in
        - The user is brought to the home page      */

    @Test
    public void NewAccount(){
    SignUpPage signUpPage = new SignUpPage(getWebDriver());
    String message = null;
    int MAXVALUE = 10000;
    int randNum = (int) (Math.random() * MAXVALUE);
    String mailUni = "plex" + randNum + "@mailinator.com";
    String strongPassword = "pwMailInator%";

    try {
            message = signUpPage.signUp(mailUni, strongPassword);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    assertTrue(message.contains("Home"));
    System.out.println("User Account is created.");
    }


}
