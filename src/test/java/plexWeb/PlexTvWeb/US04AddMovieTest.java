package plexWeb.PlexTvWeb;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import webPages.LoginPage;
import plexWeb.utils.BaseTests;

import static org.junit.Assert.assertEquals;
import static plexWeb.utils.Screenshot.screenshot;

public class US04AddMovieTest extends BaseTests {

    /* US4. As a user, I want an be able to add a movie to my watchlist.
    o	Context: A user should be able to add a movie to his watchlist from the detail page.
    o	Acceptance criteria:
        	A button is shown on the detail page to add the movie to the watchlist
        	The movie is added to the watchlist after clicking the button
        	The button will change to remove from watchlist
     */

    private WebDriver driver;

    @Before
    public void setup(){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\MeSn\\Documents\\drivers\\chromedriver87.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-maximized");
        driver = new ChromeDriver(options);
        driver.get("https://www.plex.tv/");
    }

    @Test
    public void AddMovie() throws InterruptedException {
    String mailadres = "plex-user@mailinator.com";
    String password = "password2020";

    LoginPage login = new LoginPage(driver);
    login.login();

    // Add movie to WatchList
        String movieTitleAdd = addMovie();

    // Open and Check WatchList
        checkWatchList(movieTitleAdd);

    // Click Button: remove from WatchList
        btnRemoveFromWatchList(movieTitleAdd);
    }

    @After
    public void tearDown (){
        driver.close();
    }

    public String addMovie() throws InterruptedException {
    WebDriverWait wait = new WebDriverWait(driver, 10);
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@href=\"https://www.plex.tv/watch-free/\"]")));

    // Goto movies
        Actions action = new Actions(driver);
        WebElement onDemand = driver.findElement(By.xpath("//a[@href=\"https://www.plex.tv/watch-free/\"]"));
        WebElement moviesShows = driver.findElement(By.xpath("//a[@href=\"https://mediaverse.plex.tv\"]"));
        action.moveToElement(onDemand).moveToElement(moviesShows).click().build().perform();

    // Click op eerste film
        WebElement film = driver.findElement(By.xpath("//*[@id='app']/div/div[2]/div[3]/div[2]/div/ul/li[1]"));
        film.click();

    // Add to WatchList
       WebElement addWatchlist = driver.findElement(By.xpath("//*[@id='app']/div/div[2]/div/div[2]/button[1]"));
       addWatchlist.click();

    // Save movie title added
        // todo (staat ook in MoviesPage)
       WebElement moviePage = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[2]/div[1]/h1"));
       String movieTitleAdd = moviePage.getText();
    return movieTitleAdd;
    }

    public void openMyWatchList(){
        WebElement btnMyWatchlist = driver.findElement(By.xpath("//a[@href=\"/watchlist\"]"));
        btnMyWatchlist.click();

        //Wacht op eerste film
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"app\"]/div/div[2]/ul/li[1]/a")));
    }

    public void checkWatchList (String movieTitleAdd){
       openMyWatchList();

       // open detail pagina van de eerste film in Watchlist
       WebElement myWatchListElement = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[2]/ul/li[1]/a"));
       myWatchListElement.click();

       // wacht op de film title
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"app\"]/div/div[2]/div[1]/h1")));

        // sla de film title op
       WebElement moviePage = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[2]/div[1]/h1"));
       String movieTitleAdded = moviePage.getText();

       assertEquals(movieTitleAdd, movieTitleAdded);
        screenshot(driver, "US04-AddToWatchlist");
    }

    public void btnRemoveFromWatchList(String movieTitleAdd){
        // vind remove knop
        WebElement removeBtn = driver.findElement(By.xpath("*//button[@data-testid='Watchlist-button']"));

        //Create object 'action' of an Actions class
        Actions action = new Actions(driver);
        //Mouseover on an element
        action.moveToElement(removeBtn).perform();

        // wacht op tooltip
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"app\"]/div/div[2]/div[1]/div[2]/button[1]/span")));

        WebElement toolTipRemove = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[2]/div[1]/div[2]/button[1]/span"));
        String spanRemove = toolTipRemove.getText();

        assertEquals("Remove From Watchlist", spanRemove);
        screenshot(driver, "US04-RemoveFromWatchlist");

        // verwijder film van Watchlist
        removeBtn.click();
    }

}
