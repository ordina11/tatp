package plexWeb.PlexTvWeb;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.ProfilesIni;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;

import static org.junit.Assert.assertEquals;
import static plexWeb.utils.Screenshot.screenshot;

public class US05FirefoxTest {
    // https://www.toolsqa.com/selenium-webdriver/custom-firefox-profile/
    //https://stackoverflow.com/questions/52464598/how-can-i-set-a-default-profile-for-the-firefox-driver-in-selenium-webdriver-3

    private final String mailadres = "plex-user@mailinator.com";
    private final String pw = "password2020";

    private final By btnAccept = By.xpath("//a[contains(text(),'Accept')]");
    private final By btnSignIn = By.xpath("//a[@class='signin']");
    private final By btnSubmit = By.xpath("//button[@type='submit']");
    private final By tbMail = By.id("email");
    private final By tbPassword = By.id("password");

    WebDriver driver;
    WebDriverWait wait;

    @Before
    public void setup() throws MalformedURLException {
      /*  System.setProperty("webdriver.gecko.driver", "C:\\Users\\MeSn\\Documents\\drivers\\geckodriver-v0.19.1.exe");
        ProfilesIni profileIni = new ProfilesIni();
        FirefoxProfile profile = profileIni.getProfile("profileToolsQA");
        FirefoxOptions options = new FirefoxOptions();
        options.setProfile(profile);

        WebDriver driver = new FirefoxDriver(options);
        driver.manage().window().maximize();
        wait = new WebDriverWait(driver, 20);
        driver.get("https://www.plex.tv/");*/



   /*     System.setProperty("webdriver.gecko.driver","C:\\Users\\MeSn\\Documents\\drivers\\geckodriver-v0.19.1.exe"); // Setting system properties of FirefoxDriver
        WebDriver driver = new FirefoxDriver(); //Creating an object of FirefoxDriver
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        driver.get("https://www.google.com/");*/

        System.setProperty("webdriver.gecko.driver", "C:/Users/MeSn/Documents/drivers/geckodriver-v0.19.1.exe");
        ProfilesIni profileIni = new ProfilesIni();
        FirefoxProfile profile = profileIni.getProfile("profileToolsQA");
        FirefoxOptions options = new FirefoxOptions();
        options.setProfile(profile);
        WebDriver driver = new FirefoxDriver(options);
        driver.get("http://www.google.com");
        System.out.println(driver.getTitle());


    }

    @Ignore
    @Test
    public void AddMovie() throws InterruptedException {
    String movieTitleAdd;

    login();

    // Add movie to WatchList
    movieTitleAdd = addMovieToWatchlist();

    // Open and Check WatchList
    checkWatchList(movieTitleAdd);

    // Click Button: remove from WatchList
    btnRemoveFromWatchList(movieTitleAdd);
    }

    @After
    public void tearDown (){
        driver.close();
    }

    public void login(){
    // Wacht & Click op Accept Btn
    wait.until(ExpectedConditions.elementToBeClickable(btnAccept));
    WebElement acceptBtn = driver.findElement(btnAccept);
    acceptBtn.click();

    // Wacht
    wait.until(ExpectedConditions.elementToBeClickable(btnSignIn));

    // SignIn knop
    WebElement signInBtn = driver.findElement(btnSignIn);
    signInBtn.click();

    // iFrame
    wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("fedauth-iFrame"));

    // input email
    wait.until(ExpectedConditions.elementToBeClickable(tbMail));
    WebElement mailtxtbox = driver.findElement(tbMail);
    mailtxtbox.sendKeys(mailadres);

    // input password
    WebElement passwordField = driver.findElement(tbPassword);
    passwordField.sendKeys(pw);

    // submit
    WebElement createAccountBtn = driver.findElement(btnSubmit);
    createAccountBtn.click();
    }

    public String addMovieToWatchlist() throws InterruptedException {
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@href=\"https://www.plex.tv/watch-free/\"]")));

        // Goto movies
        Actions action = new Actions(driver);
        WebElement onDemand = driver.findElement(By.xpath("//a[@href=\"https://www.plex.tv/watch-free/\"]"));
        WebElement moviesShows = driver.findElement(By.xpath("//a[@href=\"https://mediaverse.plex.tv\"]"));
        action.moveToElement(onDemand).moveToElement(moviesShows).click().build().perform();

        // Click op eerste film
        WebElement film = driver.findElement(By.xpath("//*[@id='app']/div/div[2]/div[3]/div[2]/div/ul/li[1]"));
        film.click();

        // Add to WatchList
        WebElement addWatchlist = driver.findElement(By.xpath("//*[@id='app']/div/div[2]/div/div[2]/button[1]"));
        addWatchlist.click();

        // Save movie title added
        // todo (staat ook in MoviesPage)
        WebElement moviePage = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[2]/div[1]/h1"));
        String movieTitleAdd = moviePage.getText();
        return movieTitleAdd;
    }

    public void openMyWatchList(){
        WebElement btnMyWatchlist = driver.findElement(By.xpath("//a[@href=\"/watchlist\"]"));
        btnMyWatchlist.click();

        //Wacht op eerste film
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"app\"]/div/div[2]/ul/li[1]/a")));
    }

    public void checkWatchList (String movieTitleAdd){
       openMyWatchList();

       // open detail pagina van de eerste film in Watchlist
       WebElement myWatchListElement = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[2]/ul/li[1]/a"));
       myWatchListElement.click();

       // wacht op de film title
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"app\"]/div/div[2]/div[1]/h1")));

        // sla de film title op
       WebElement moviePage = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[2]/div[1]/h1"));
       String movieTitleAdded = moviePage.getText();

       assertEquals(movieTitleAdd, movieTitleAdded);
        screenshot(driver, "UC05-screenshot1");
    }

    public void btnRemoveFromWatchList(String movieTitleAdd){
        // vind remove knop
        WebElement removeBtn = driver.findElement(By.xpath("*//button[@data-testid='Watchlist-button']"));

        //Create object 'action' of an Actions class
        Actions action = new Actions(driver);
        //Mouseover on an element
        action.moveToElement(removeBtn).perform();

        // wacht op tooltip
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"app\"]/div/div[2]/div[1]/div[2]/button[1]/span")));

        WebElement toolTipRemove = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div[2]/div[1]/div[2]/button[1]/span"));
        String spanRemove = toolTipRemove.getText();

        assertEquals("Remove From Watchlist", spanRemove);
        screenshot(driver, "UC05-screenshot2");

        // verwijder film van Watchlist
        removeBtn.click();
    }

}
