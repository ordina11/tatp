package plexWeb.PlexTvWeb;

import webPages.LoginPage;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import plexWeb.utils.BaseTests;
import static org.junit.Assert.assertTrue;
import static plexWeb.utils.Screenshot.screenshot;

public class US02StartStreamingTest extends BaseTests {
    /* US2. As a user, I want to be able to start a stream directly from the home page.
        Context: On the home page, a selection of movies will be shown. It should be possible for a user to start a movie by clicking the play button in the thumbnail.
	    Acceptance criteria:
            	Clicking the play button will start the video player
            	The movie starts streaming     */

    private final By diveInBtn = By.xpath("//*[@id='plex-site-container']/div[4]/div[2]/section/div[2]/div/div[2]/a/div/span");
    private final By steamFreeBtn = By.xpath("//*[@id=\"plex-site-container\"]/div[4]/div[2]/section/div/div/a");
    private final By selectMovieBtn = By.xpath("//*[@id='app']/div/div[2]/div[2]/div[2]/div/ul/li[1]/a/div/div");
    private final By watchNowBtn = By.xpath("//*[@id='app']/div/div[2]/div[1]/div[2]/a");
    private final By player = By.xpath("//*[@id=\"plex\"]");

    @Test
    public void startSteaming() throws InterruptedException {
        WebDriver webDriver = getWebDriver();
        WebDriverWait wait = new WebDriverWait(webDriver , 20);

        LoginPage login = new LoginPage(webDriver);
        login.login();

        // home page: click Dive In
        wait.until(ExpectedConditions.elementToBeClickable(diveInBtn));
        WebElement btnDiveIn = webDriver.findElement(diveInBtn);
        btnDiveIn.click();

        // click play btn
        wait.until(ExpectedConditions.elementToBeClickable(steamFreeBtn));
        WebElement btnSteamFree = webDriver.findElement(steamFreeBtn);
        btnSteamFree.click();

        // Click film
        WebElement btnSelectMovie = webDriver.findElement(selectMovieBtn);
        btnSelectMovie.click();

        // Watch Now
        WebElement btnWatchNow = webDriver.findElement(watchNowBtn);
        btnWatchNow.click();

        // assert URL Player
        wait.until(ExpectedConditions.elementToBeClickable(player));
        Boolean playerDisplayed = webDriver.findElement(player).isDisplayed();
        assertTrue(playerDisplayed);
        screenshot(webDriver, "US02-StartMovie");
    }
}
