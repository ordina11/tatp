package plexWeb.utils;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import utils.Links;
import utils.webDriverPage;

public class BaseTests {

     private static WebDriver webDriver;
     protected static webDriverPage homePage;

     @BeforeClass
     public static void launchApplication(){
     setChromeDriverProperty();
     ChromeOptions options = new ChromeOptions();
     options.addArguments("start-maximized");
     webDriver = new ChromeDriver(options);
     webDriver.get(Links.HOME);
     homePage = new webDriverPage(webDriver);
     }

     @AfterClass
     public static void closeBrowser(){
            webDriver.quit();
        }

     public static WebDriver getWebDriver(){
            return webDriver;
        }

     private static void setChromeDriverProperty(){
       System.setProperty("webdriver.chrome.driver", "C:\\Users\\MeSn\\Documents\\drivers\\chromedriver87.exe");
     }


    }







