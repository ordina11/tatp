package plexMob.pagesMob;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class mobStreamPage {


    public static By elementToWait(WebDriver driver){
        return By.id("com.plexapp.android:id/play");
    }

    public static WebElement btn_PlayMovie (WebDriver driver){
        return driver.findElement(By.id("com.plexapp.android:id/play"));
    }

    public static WebElement lyo_PlayMovie (WebDriver driver){
        return driver.findElement(By.id("com.plexapp.android:id/layout"));
    }

    public static WebElement view_PlayContentMovie (WebDriver driver){
        return driver.findElement(By.id("com.plexapp.android:id/player_content_view"));
    }

}
