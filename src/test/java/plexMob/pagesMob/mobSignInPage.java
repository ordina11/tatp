package plexMob.pagesMob;

import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class mobSignInPage {


    public static WebElement banner_SignIn (WebDriver driver){
        return driver.findElement(By.id("com.plexapp.android:id/banner"));
    }

    public static WebElement btn_UpsellSignIn (WebDriver driver){
        return driver.findElement(By.id("com.plexapp.android:id/upsell_signin"));
    }

    public static By WaitOnSelectSignMethod(WebDriver driver){
               return By.id("com.plexapp.android:id/continue_with_email");
        }

    public static WebElement btn_ContinueWithMail (WebDriver driver){
        return driver.findElement(By.id("com.plexapp.android:id/continue_with_email"));
    }
     public static WebElement sel_User (WebDriver driver){
        return driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.ListView/android.widget.RelativeLayout[1]/android.widget.TextView[1]"));
    }
    public static By WaitOnInputfields(WebDriver driver){
        return By.id("com.plexapp.android:id/username");
    }

    public static By WaitOnBtnSignInUser(WebDriver driver){
        return MobileBy.AccessibilityId("Sign in");
    }

    public static WebElement btn_SignInUser (WebDriver driver){
        return driver.findElement(MobileBy.AccessibilityId("Sign in"));
    }

    public static WebElement txt_UserName (WebDriver driver) {
        return driver.findElement(By.id("com.plexapp.android:id/username"));
    }

    public static WebElement txt_UserName2 (WebDriver driver) {
        return driver.findElement(By.id("com.plexapp.android:id/textinput_placeholder"));
    }

    public static WebElement txt_Password (WebDriver driver) {
        return driver.findElement(By.id("com.plexapp.android:id/password"));
    }

    public static WebElement btn_SignIn (WebDriver driver){
        return driver.findElement(By.id("com.plexapp.android:id/sign_in"));
    }

    public static WebElement btn_GglGotIt (WebDriver driver){
        return driver.findElement(By.id("android:id/button1"));
    }

    //  public static By elementToWait(WebDriver driver){
 //       return By.id("Poster");
   // }

}
