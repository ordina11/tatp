package plexMob.pagesMob;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class mobMovieDetailPage {


    public static WebElement btn_ResumeMovie (WebDriver driver){
        return driver.findElement(By.id("com.plexapp.android:id/promoted_action"));
    }

    public static By elementToWait(WebDriver driver){
        return By.id("com.plexapp.android:id/promoted_action");
    }

    public static WebElement opt_PlayFromBegin (WebDriver driver){
        return driver.findElement(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[2]"));
    }

    public static By popupToWait(WebDriver driver){
        return By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[2]");
    }




}
