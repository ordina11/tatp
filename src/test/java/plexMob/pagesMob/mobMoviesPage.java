package plexMob.pagesMob;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class mobMoviesPage {

    public static WebElement btn_SelectMovie (WebDriver driver){
        return driver.findElement(By.id("Poster"));
    }

    public static By elementToWait(WebDriver driver){
        return By.id("Poster");
    }

}
