package plexMob;

import io.appium.java_client.android.AndroidDriver;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import plexMob.pagesMob.*;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.Assert.assertTrue;
import static plexWeb.utils.Screenshot.screenshot;

public class playMovieTest {
    AndroidDriver driver;
    WebDriverWait wait;

    @Before
    public void setUp() throws MalformedURLException {

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", "11.0");

     //   capabilities.setCapability("noReset", "true");
        capabilities.setCapability("automationName", "UiAutomator2");

        capabilities.setCapability("appPackage", "com.plexapp.android");
        capabilities.setCapability("appActivity", "com.plexapp.plex.activities.SplashActivity");

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
        wait = new WebDriverWait(driver, 100);

        //Open Plex app
       // driver.findElement(MobileBy.AccessibilityId("Plex")).click();
    }

    @Ignore
    @Test
    public void actualTest() throws Exception{
        wait.until(ExpectedConditions.invisibilityOfElementLocated(mobDriverPage.waitStartAnimation(driver)));

        wait.until(ExpectedConditions.presenceOfElementLocated(mobHomePage.waitOnMovieDashboard(driver)));

        boolean SignInBanner = mobSignInPage.banner_SignIn(driver).isDisplayed();
        //SignIn
        if(SignInBanner = false){
            mobHomePage.menu_Open(driver).click();
            mobHomePage.menu_SignIn(driver).click();

        }else {
            // Sign in
            mobSignInPage.btn_UpsellSignIn(driver).click();
        }

        // Sign
        wait.until(ExpectedConditions.presenceOfElementLocated(mobSignInPage.WaitOnSelectSignMethod(driver)));
        mobSignInPage.btn_ContinueWithMail(driver).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(mobSignInPage.WaitOnInputfields(driver)));

        // oSuchElementException: An element could not be located on the page using the given search parameters.
        //{Using=xpath, value=/hierarchy/android.widget.FrameLayout/android.widget.ListView/android.widget.RelativeLayout[1]/android.widget.TextView[1]}
        if (mobSignInPage.sel_User(driver).isDisplayed()){
            mobSignInPage.sel_User(driver).click();
            if(mobSignInPage.btn_GglGotIt(driver).isDisplayed()){
                mobSignInPage.btn_GglGotIt(driver).click();
            }
        }
        else {
        mobSignInPage.txt_UserName2(driver).sendKeys("plex-user@mailinator.com");
        mobSignInPage.txt_Password(driver).sendKeys("password2020");
        }

        // Btn SignIn
        wait.until(ExpectedConditions.presenceOfElementLocated(mobSignInPage.WaitOnBtnSignInUser(driver)));
        mobSignInPage.btn_SignInUser(driver).click();

        // Open Menu
        wait.until(ExpectedConditions.presenceOfElementLocated(mobHomePage.waitOnMostPopular(driver)));
        mobHomePage.menu_Open(driver).click();

        //Menu - Go to Movies & Shows
        wait.until(ExpectedConditions.presenceOfElementLocated(mobHomePage.waitOnDropdown(driver)));
        mobHomePage.ddl_ShowsAndMovies(driver).click();

        // Press Movie
        mobMoviesPage.btn_SelectMovie(driver).click();

        // Start/Resume Movie
        wait.until(ExpectedConditions.presenceOfElementLocated(mobMovieDetailPage.elementToWait(driver)));
        mobMovieDetailPage.btn_ResumeMovie(driver).click();

        // Popup Play from beginning
        wait.until(ExpectedConditions.presenceOfElementLocated(mobMovieDetailPage.popupToWait(driver)));
        mobMovieDetailPage.opt_PlayFromBegin(driver).click();

        // Streaming - assert Movie is playing
        wait.until(ExpectedConditions.presenceOfElementLocated(mobStreamPage.elementToWait(driver)));

        mobStreamPage.lyo_PlayMovie(driver).click(); // click layout
        mobStreamPage.view_PlayContentMovie(driver).click(); // click Player content view
        wait.until(ExpectedConditions.presenceOfElementLocated(mobMovieDetailPage.popupToWait(driver)));

        boolean playBtnDisplayed = mobStreamPage.btn_PlayMovie(driver).isDisplayed();
        assertTrue("Movie is playing", playBtnDisplayed);
        screenshot(driver, "mob-PlayMovie-screenshot");

        // Go Back (stop Playing)
      //  driver.navigate().back();
       // driver.findElementById("com.plexapp.android:id/promoted_action").click();

        //driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ListView/android.widget.TextView[2]").click();

        driver.navigate().back();
        driver.findElementById("com.plexapp.android:id/mp__stop").click();
        driver.closeApp();



    }

    @After
    public void tearDown() {
        driver.closeApp();
        driver.quit();
    }


}
